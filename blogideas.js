Blogs = new Mongo.Collection("blogs");
Ideas = new Mongo.Collection("ideas");

if (Meteor.isClient) {

	Template.list.helpers({
		my_ideas: function () {
			return Ideas.find({});
		},

		allblogs: function () {
			return Blogs.find({});
		}
	});

	Template.list.events({
		"click .toggle-checked": function () {
			// body...
		}
	});

	Template.blogroll.helpers({
		my_blogs: function () {
			return Blogs.find({});
		}
	});

	Template.blogroll.events({
		"submit .newblog": function (event) {
			// prevent browser default behaviour
			event.preventDefault();

			// log input
			console.log(event);

			// get value from form element
			var blog_to_enter = event.target.blogname.value;

			// insert a blog into the collection
			Blogs.insert({
				blog: blog_to_enter,
				created: new Date()
			});

			// clear form
			event.target.blogname.value = "";
		}, 
		"click .delete": function () {
			Blogs.remove(this._id);
		}
	});
}

if (Meteor.isServer) {
	Meteor.startup(function () {
		// code to run on server at startup
	});
}